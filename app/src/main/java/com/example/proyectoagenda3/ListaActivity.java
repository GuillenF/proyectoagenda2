package com.example.proyectoagenda3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListaActivity extends AppCompatActivity {
    TableLayout tblLista;
    ArrayList<Contacto> contactos;
    ArrayList<Contacto> filtercontactos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        tblLista = (TableLayout) findViewById(R.id.tblLista);
        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");
        filtercontactos = contactos;
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContactos();

    }
    public void cargarContactos() {
        for(int x=0; x < contactos.size();x++) {
            Contacto c= new Contacto(contactos.get(x));
            TableRow nRow=new TableRow(ListaActivity.this);
            TextView nText=new TextView(ListaActivity.this);
            nText.setText(c.getNombre());
            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nText.setTextColor((c.isFavorite())? Color.BLUE:Color.BLACK);
            nRow.addView(nText);
            Button nButton = new Button(ListaActivity.this);
            nButton.setText(R.string.accion_ver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nButton.setTextColor(Color.BLACK);
            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacto c=(Contacto)v.getTag(R.string.contacto_g);
                    Intent i= new Intent();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto",c);
                    oBundle.putInt("index",Integer.valueOf(v.getTag(R.string.contacto_g_index).toString()));
                    i.putExtras(oBundle);setResult(RESULT_OK,i);
                    finish();
                }
            });
            Button btnEliminar = new Button(ListaActivity.this);
            btnEliminar.setText("Eliminar");
            btnEliminar.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            btnEliminar.setTextColor(Color.BLACK);
            btnEliminar.setOnClickListener(this.btnEliminarsAction(c.get_ID()));
            btnEliminar.setTag(R.string.contacto_g_index, c.get_ID());
            nRow.addView(btnEliminar);
            nButton.setTag(R.string.contacto_g,c);
            nButton.setTag(R.string.contacto_g_index,x);
            nRow.addView(nButton);
            tblLista.addView(nRow);
        }
    }

    private void deleteContacto(long id) {
        Contacto c = new Contacto();
        for(int x = 0; x < filtercontactos.size(); x++) {
            if(filtercontactos.get(x).get_ID() == id) {
                this.filtercontactos.remove(x);
                Toast.makeText(this, "Se elimino el contacto",Toast.LENGTH_SHORT).show();
                break;
            }
        }
        this.contactos = filtercontactos;
        tblLista.removeAllViews();
        cargarContactos();
    }

    public View.OnClickListener btnEliminarsAction(final long id) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("ID", String.valueOf(id));
                deleteContacto(id);
            }
        };
    }
    public void buscar(String s){
        ArrayList<Contacto> list = new ArrayList<>();
        for (int x = 0; x < filtercontactos.size();x++){
            if (filtercontactos.get(x).getNombre().contains(s))
                list.add(filtercontactos.get(x));
        }
        contactos = list;
        tblLista.removeAllViews();
        cargarContactos();
    }
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);

        MenuItem menuItem = menu.findItem(R.id.menu_search);

        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                buscar(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
}